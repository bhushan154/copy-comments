/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jiraplugins.postfunctions.copycomments;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import java.util.List;
import java.util.Map;
import com.atlassian.jira.issue.MutableIssue;


/*
This is the post-function class that gets executed at the end of the transition.
Any parameters that were saved in your factory class will be available in the transientVars Map.
 */

public class SubtaskToParent extends AbstractJiraFunctionProvider{
    private static final Logger log = LoggerFactory.getLogger(SubtaskToParent.class);    

    public void execute(Map transientVars,Map args,PropertySet ps)throws WorkflowException{
        MutableIssue issue=getIssue(transientVars);
        CommentManager commentManager = ComponentAccessor.getCommentManager();
        try
        {
             if(issue.isSubTask())
             {
                 List<Comment> comments = commentManager.getComments(issue);
                 if(comments!=null && comments.size()>0)
                 {
                     for(Comment comment:comments)
                     {
                         commentManager.create(issue.getParentObject(),comment.getAuthor(),comment.getBody(),true);
                     }
                 }
             }
        }
        catch(Exception exc)
        {
            log.error("Copy Comment Plugin Error: Error copying comments from sub-task to parent task. " + exc.toString());
        }
    }
}